using System;
using IvisionStudioConstants;

namespace WK260 {
    [Serializable]
    public class DeviceOperationException : Exception {
        public DeviceErrorCodes ErrorCodes { get; }
        
        public DeviceOperationException ()
        {}

        public DeviceOperationException(DeviceErrorCodes errorCodes) : base() {
            ErrorCodes = errorCodes;
        }

        public DeviceOperationException (string message) 
            : base(message)
        {}

        public DeviceOperationException(string message, DeviceErrorCodes errorCodes)
            : base(message) {
            ErrorCodes = errorCodes;
            
        }

        public DeviceOperationException (string message, Exception innerException)
            : base (message, innerException)
        {}    
    }
}