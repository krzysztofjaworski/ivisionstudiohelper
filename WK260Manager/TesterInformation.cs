﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WK260 {
    public class TesterInformation {
        public TesterInformation(string firmwareVersion, string fPgaVersion, string srnNumber) {
            FirmwareVersion = firmwareVersion;
            FPGAVersion = fPgaVersion;
            SrnNumber = srnNumber;
        }

        public string FirmwareVersion { get; }
        public string FPGAVersion { get; }
        public string SrnNumber { get; }
    }
}
