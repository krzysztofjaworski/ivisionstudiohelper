﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading;
using HelperLibrary.Utils;
using IvisionStudioCompiler;
using IvisionStudioConstants;
using IvisionStudioFtpClient;
using IvisionStudioHardwareAccess;
using IvisionStudioTE.Library;
using TDModules;

namespace WK260 {
    public class DeviceManager {

        private readonly TELibrary _testLibrary;
        private WK260Device _device;

        public bool IsConnected {
            get {
                return _isConnected;
            }
            private set {
                if (value == _isConnected) return;
                _isConnected = value;
                OnConnectionStateChanged(value ? GetTesterInformation() : null);
            }
        }
        public bool IsProbeActive { get; private set; }

        private bool _isConnected;

        public event EventHandler<TesterInformation> ConnectionStateChanged;

        public event EventHandler<string> OnError;

        private void OnConnectionStateChanged(TesterInformation e) {
            ConnectionStateChanged?.Invoke(null, e);
        }


        public DeviceManager(string systemParameters) {
            _testLibrary = new TELibrary();
            _testLibrary.LoadSystemParameters(systemParameters);
            _testLibrary.OnException = OnException;
            IsConnected = true;
            //_testLibrary.OnDeviceConnect += OnDeviceConnect;
            //_testLibrary.OnDeviceDisconnect += OnDeviceDisconnect;
        }

        private void OnException(Exception exception, string message) {
            OnError?.Invoke(this, message);
        }


        public bool ConnectDevice() {
            _device = new WK260Device(IPAddress.Parse("192.168.215.10"), 2048);
            _device.Connect("192.168.215.10");
            if (!_device.IsConnected) return false;
            OnConnectionStateChanged(GetTesterInformation());
            return true;

        }

        // ReSharper disable once UnusedMember.Local
        private void OnDeviceDisconnect() {
            IsConnected = false;
            OnConnectionStateChanged(null);
        }

        // ReSharper disable once UnusedMember.Local
        private void OnDeviceConnect() {
            IsConnected = true;
            OnConnectionStateChanged(GetTesterInformation());
        }

        // ReSharper disable once UnusedMember.Global
        public void ReloadSystemParameters(string systemParameters) {
            _testLibrary.LoadSystemParameters(systemParameters);
        }

        public bool CompileProject(string path) {
            TestDeskModuleCache _cache = new TestDeskModuleCache();
            IvisionStudioCompiler.Compiler _compiler = new Compiler(_cache, _testLibrary.SystemParameters.SystemParameterBase);
            var _project = new IvisionStudio.Common.Project.IvisionStudioProject(path);
            return _compiler.CompileProject(_project);
        }

        public void InitLeds(int range) {
            _testLibrary.InitAllPinsAsLEDs(range * 64);
        }

        public void SetLedOn(int ledAddress) {
            SetLedsOn(new[] { ledAddress });
        }

        public void SetLedsOn(IEnumerable<int> ledAddress) {
            var leds = ledAddress as int[] ?? ledAddress.ToArray();

            if (!_device.IsConnected) ConnectDevice();

            _device.SendCommandList(new ProtocolCommandList(new ProtocolCommandSetLedListOn() {
                PinAddressList = leds.Select(x => (ushort)x).ToList()
            }));

        }

        public void SetLedsOff(IEnumerable<int> ledAddresses) {

            if (!_device.IsConnected) this.ConnectDevice();

            DeviceErrorCodes _errorCodes = _device.SendCommandList(new ProtocolCommandList(new ProtocolCommandSetLedListOff() {
                PinAddressList = ledAddresses.Select(x => (ushort)x).ToList()
            }));
        }

        public void ResetLeds() {
            if (_device == null) return;
            if (!_device.IsConnected) ConnectDevice();
            if (!_device.IsConnected) OnException(new Exception(), "ERR_CONNECT");

            _device.SendCommandList(new ProtocolCommandList(new ProtocolCommandSetLedsOff()));
        }

        public void StartProbe(Action<IEnumerable<int>> onProbeCallback, string project) {
            bool _loadedProject = _testLibrary.LoadProject(project, false);
            _testLibrary.OnProbeDataReceived = (list, stream) => {
                if (list != null) {
                    List<int> _pins = new List<int>();
                    foreach (ushort pin in list) {
                        _pins.Add(pin);
                    }

                    onProbeCallback(_pins);
                } else {
                    onProbeCallback(null);
                }
            };

            Thread.SpinWait(100);
            IsProbeActive = _testLibrary.StartProbe(out string errorCode);
            if (errorCode != "ERR_NONE") OnError?.Invoke(this, errorCode);

        }

        public void StopProbe() {
            _testLibrary.StopProbe();
            IsProbeActive = false;
        }

        public TesterInformation GetTesterInformation() {
            // ReSharper disable once InconsistentNaming
            const string FTPLogin = "wEtUs3Rf7?";
            // ReSharper disable once InconsistentNaming
            const string FTPPassword = "i5Z9%cKa&8DaFuQ";
            // ReSharper disable once InconsistentNaming
            const string WK_DRIVE = "A:\\";
            //const string WK_ADJUST = "wkadjust.ini";

            if (!PingDevice("192.168.215.10")) {
                return null;
            }

            FtpServerData ftpServerData = new FtpServerData("192.168.215.10", FTPLogin, FTPPassword);
            FtpClient ftpClient = new FtpClient(ftpServerData);
            if (ftpClient.CheckConnection(out string error, 500)) {
                string path = WK_DRIVE + "wk260_type_pc.ini";
                if (ftpClient.RemoteFileExists(path)) {
                    MemoryStream _memoryStream = new MemoryStream();
                    if (ftpClient.DownloadToMemoryStream(path, _memoryStream, out error)) {
                        string[] info = Encoding.ASCII.GetString(_memoryStream.ToArray()).Split('\r', '\n')
                            .Where(x => !string.IsNullOrEmpty(x)).ToArray();
                        return GetInformation(info);
                    } else {
                        IsConnected = false;
                        throw new DeviceOperationException("Cannot download file. Reason: Unknown");
                    }
                } else {
                    IsConnected = false;
                    throw new DeviceOperationException("Cannot read information from device. Missing files.");
                }
            } else throw new DeviceOperationException("Cannot connect to ftp port. Check if device is online.");
        }

        private TesterInformation GetInformation(string[] info) {
            string firmware = info.First(x => x.StartsWith("Version=")).Replace("Version=", "");
            string serial = info.First(x => x.StartsWith("Number=")).Replace("Number=", "");
            string fpga = info.Last(x => x.StartsWith("Version=")).Replace("Version=", "");
            TesterInformation _testerInformation = new TesterInformation(firmware, fpga, serial);
            return _testerInformation;
        }

        private static bool PingDevice(string ipAddress) {
            using (new TimeMeasurement("Pinging analyzer.")) {
                Ping pingSender = new Ping();
                const int timeout = 10;
                PingReply reply;
                using (var replayMeasurement = new TimeMeasurement("Waiting for replay.")) {
                    reply = pingSender.Send("192.168.215.10", timeout);
                }

                if (reply != null && reply.Status == IPStatus.Success) {
                    Debug.WriteLine("Address: {0}", reply.Address.ToString());
                    Debug.WriteLine("RoundTrip time: {0}", reply.RoundtripTime);
                    Debug.WriteLine("Time to live: {0}", reply.Options.Ttl);
                    Debug.WriteLine("Don't fragment: {0}", reply.Options.DontFragment);
                    Debug.WriteLine("Buffer size: {0}", reply.Buffer.Length);
                    return true;
                } else {
                    if (reply != null) Debug.WriteLine(reply.Status);
                    return false;
                }
            }
        }
    }
}
