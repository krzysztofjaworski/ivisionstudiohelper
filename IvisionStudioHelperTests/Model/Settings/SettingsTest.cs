using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using IvisionStudioHelper.Model.Settings;
using NUnit.Framework;

namespace IvisionStudioHelperTests.Settings {
    [TestFixture]
    public class SettingsTest {
        [Test]
        public void LoadSettingsTest() {
            string local = ConstantSettings.IVISION_SYSTEM_PARAMETER_FILEPATH;
        }

        [Test]
        public void LoadMatrixConfigurationsTest() {
            IEnumerable<HwMatrixConfiguration> configurations = ApplicationCommonSettings.MatrixConfigurations;
            Debug.WriteLine(configurations.Count());
            Assert.IsTrue(configurations.Count() == 1);
        }
        
    }
}