using System;

namespace IvisionStudioHelper.Model.Project {
    [Serializable]
    public class ErrorPosition {
        public int DisplayLine { get; set; }
        public int DisplayCharacter { get; set; }
    }
}