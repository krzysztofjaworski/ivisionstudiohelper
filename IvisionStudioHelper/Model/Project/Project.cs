using System;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Linq;
using HelperLibrary.MVVM;
using DateTime = System.DateTime;

namespace IvisionStudioHelper.Model.Project {
    public class Project : PropertyChangedBase, ICloneable {
        public string Name { get; set; }
        public string Description { get; set; }
        public int Range { get; set; }
        public int PinRange {
            get { return Range * 64; }
        }
        public DateTime Created { get; set; }
        public string CreatedBy { get; set; }
        public DateTime LastChanged { get; set; }
        public string LastChangedBy { get; set; }
        public bool IsMatrixConfigurationDefault { get; set; }
        public string HwMatrixConfigurationName { get; set; }
        public string Path { get; private set; }
        public bool HasOwnPintable { get; private set; }
        public bool HasOwnParameter { get; private set; }
        public bool HasOwnTSL { get; private set; }
        public string ParameterPath { get; private set; } = null;
        public string PintablePath { get; private set; } = null;
        public string TCLPath { get; private set; } = null;



        public Project(string path) {
            Path = path;
        }

        public Project(string name, int range, string createdBy, string hwMatrixConfigurationName, string path) {
            Name = name;
            Range = range;
            CreatedBy = createdBy;
            LastChanged = DateTime.Now;
            Created = DateTime.Now;
            HwMatrixConfigurationName = hwMatrixConfigurationName;
            Path = path;
        }

        private Project() {

        }

        public void Initialize() {
            XDocument _document = XDocument.Load(Path);
            Name = _document.Root.Elements().SingleOrDefault(x => x.Name == "ProjectName").Value;
            Description = _document.Root.Elements().SingleOrDefault(x => x.Name == "Text")?.Value;
            Range = int.Parse(_document.Root.Elements().SingleOrDefault(x => x.Name == "PinRangeConnector")?.Value ?? throw new InvalidOperationException());
            Created = DateTime.Parse(_document.Root.Elements().SingleOrDefault(x => x.Name == "Created")?.Value);
            CreatedBy = _document.Root.Elements().SingleOrDefault(x => x.Name == "Created_by")?.Value;
            LastChanged = DateTime.Parse(_document.Root.Elements().SingleOrDefault(x => x.Name == "LastChanged")?.Value);
            LastChangedBy = _document.Root.Elements().SingleOrDefault(x => x.Name == "LastChanged_by")?.Value;
            IsMatrixConfigurationDefault = _document.Root.Elements().SingleOrDefault(x => x.Name == "IsHwMatrixConfigurationDefault")?.Value == "true";
            HwMatrixConfigurationName = _document.Root.Elements().SingleOrDefault(x => x.Name == "HwMatrixConfigurationName")?.Value;
            HasOwnPintable = CheckIfHasPintable(System.IO.Path.GetDirectoryName(Path), System.IO.Path.GetFileNameWithoutExtension(Path));
            HasOwnParameter = CheckIfHasParameter(System.IO.Path.GetDirectoryName(Path), System.IO.Path.GetFileNameWithoutExtension(Path));
            HasOwnTSL = CheckIfHasTSL(System.IO.Path.GetDirectoryName(Path), System.IO.Path.GetFileNameWithoutExtension(Path));
        }
        

        private void InitializePaths() {

        }

        private bool CheckIfHasParameter(string path, string name) {
            string parameterPath = $"{path}\\{name}.parameter";
            bool _return = System.IO.File.Exists(parameterPath);
            if (_return) ParameterPath = parameterPath;
            return _return;
        }

        private bool CheckIfHasTSL(string path, string name) {
            string tclPath = $"{path}\\{name}.tsl";
            bool _return = System.IO.File.Exists(tclPath);
            if (_return) TCLPath = tclPath;
            return _return;
        }

        private bool CheckIfHasPintable(string path, string name) {
            string printablePath = $"{path}\\{name}.pintable";
            bool _return = System.IO.File.Exists(printablePath);
            if (_return) PintablePath = printablePath;
            return _return;
        }

        public Task InitializeAsync() {
            return Task.Factory.StartNew(Initialize);
        }

        public object Clone() {
            return new Project() {
                Name = this.Name.Clone().ToString(),
                Description = this.Description.Clone().ToString(),
                Range = this.Range,
                Created = this.Created,
                CreatedBy = this.CreatedBy,
                Path = this.Path.Clone().ToString(),
                HasOwnPintable = this.HasOwnPintable,
                HwMatrixConfigurationName = this.HwMatrixConfigurationName.Clone().ToString(),
                HasOwnParameter = this.HasOwnParameter,
                LastChanged = this.LastChanged,
                IsMatrixConfigurationDefault = this.IsMatrixConfigurationDefault,
                LastChangedBy = this.LastChangedBy
            };
        }

        public override string ToString() {
            return this.Name;
        }
    }
}