using System;
using System.Xml.Serialization;
using HelperLibrary.MVVM;

namespace IvisionStudioHelper.Model.Project {
    [Serializable]
    [XmlRoot("IvisionError")]
    public class ProjectError : PropertyChangedBase {
        public string Description { get; set; }
        public string Level { get; set; }
        public string ErrorType { get; set; }
        public string TestPoint { get; set; }
        public int PinAddress { get; set; }
        public string TslCode { get; set; }
        
        public AsyncObservableCollection<ProjectError> FirstPosition { get; set; }
        public AsyncObservableCollection<ProjectError> EndPosition { get; set; }
    }
}