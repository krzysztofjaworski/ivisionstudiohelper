using System.Collections.Generic;
using ICSharpCode.AvalonEdit.Highlighting;

namespace IvisionStudioHelper.Model.Settings {
    public static class ApplicationCommonSettings {
        /// <summary>
        /// Keep reference to WK260 device class manager.
        /// </summary>
        public static WK260.DeviceManager DeviceManager;
        /// <summary>
        /// Keep highlighting definition for AvalonEdit.
        /// </summary>
        public static IHighlightingDefinition HighlightingDefinition { get; set; }
        /// <summary>
        /// Keep collection of MatrixConfigurations IvisionStudio.
        /// </summary>
        public static IEnumerable<HwMatrixConfiguration> MatrixConfigurations { get; set; }
    }
}