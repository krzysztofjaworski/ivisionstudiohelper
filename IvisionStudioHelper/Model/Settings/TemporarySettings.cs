using IvisionStudioHelper.Model.Pintable;

namespace IvisionStudioHelper.Model.Settings {
    public static class TemporarySettings {
        public static AutoInputData LastAutoInputData { get; set; }
        public static SearchPintableData LastSearchedPintableData { get; set; }
        public static QuickInputData LastQuickInputData { get; set; }
    }
}