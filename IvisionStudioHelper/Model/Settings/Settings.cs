using System;
using System.Configuration;

   
namespace IvisionStudioHelper.Model.Settings {
    [Serializable]   
    public class Settings {
        public string IvisionStudioPath { get; set; } = @"C:\Program Files (x86)\Weetech GmbH\IVISion Studio";
        public string LastOpenedPath { get; set; } = string.Empty;
    }
}