using System;
using System.IO;

namespace IvisionStudioHelper.Model.Settings {
    public static class ConstantSettings {
        
        /// <summary>
        /// Settings to IvisionStudio Settings dir
        /// </summary>
        public static readonly string IVISION_SETTINGS_DIR = Path.Combine(IVISION_COMMON_DIR(), "Settings");
        
        /// <summary>
        /// File name without path of System Parameters.
        /// </summary>
        public static readonly string IVISION_SYSTEM_PARAMETER_FILENAME = "SystemParameter.param";
        
        /// <summary>
        /// Full path to System parameter file.
        /// </summary>
        public static readonly string IVISION_SYSTEM_PARAMETER_FILEPATH =
            Path.Combine(IVISION_SETTINGS_DIR, IVISION_SYSTEM_PARAMETER_FILENAME);
        
        /// <summary>
        /// Get common path dir for IvisionStudio files.
        /// </summary>
        /// <returns></returns>
        public static string IVISION_COMMON_DIR() => Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonDocuments), "Weetech", "IvisionStudio");
        
        
        /// <summary>
        /// Get common dir for this application.
        /// </summary>
        public static string APP_SETTINGS_DIR { get; set; } =
            Environment.GetFolderPath(Environment.SpecialFolder.CommonDocuments) + "\\IvisionStudioHelper";
        
        /// <summary>
        /// Path to settings of this application.
        /// </summary>
        public static readonly string APP_SETTINGS_FILEPATH = $"{ConstantSettings.APP_SETTINGS_DIR}\\Settings.dat";
    }
}