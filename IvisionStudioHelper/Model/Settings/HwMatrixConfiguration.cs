namespace IvisionStudioHelper.Model.Settings {
    public class HwMatrixConfiguration {
        
        public string Name { get; set; }
        public string Path { get; private set; }
        
        public HwMatrixConfiguration(string pathDirectory) {
            Path = pathDirectory;
            Name = System.IO.Path.GetFileNameWithoutExtension(pathDirectory);
        }        
    }
}