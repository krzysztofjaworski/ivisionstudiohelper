using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using ICSharpCode.AvalonEdit.Highlighting;
using IvisionStudioHelper.Model.Pintable;

namespace IvisionStudioHelper.Model.Settings {
    /// <summary>
    /// Settings class. Is it responsible for load, save and keep settings of application.
    /// </summary>
    [Serializable]
    public static class ConfigurationManager {
        public static Settings ApplicationSettings;

        static ConfigurationManager() {
            if (!Load()) {
                HelperLibrary.Utils.DebugHelper.DebugWriteLine("Loading default settings.");
                ApplicationSettings = new Settings();
            }
            else {
                HelperLibrary.Utils.DebugHelper.DebugWriteLine("Loading settings successfully.");
            }

            LoadHighlightDefinition();
            ApplicationCommonSettings.MatrixConfigurations = GetMatrixConfigurations();
        }

        private static bool Load() {
            HelperLibrary.Utils.DebugHelper.DebugWriteLine("Loading settings...");
            if (!CheckFile()) {
                HelperLibrary.Utils.DebugHelper.DebugWriteLine("Settings file do not exist.");
                return false;
            }
                
            try {
                HelperLibrary.Utils.DebugHelper.DebugWriteLine("File exist. Trying loading settings...");
                ApplicationSettings = HelperLibrary.Serializers.BinarySerializer<Settings>.DeserializeData(ConstantSettings.APP_SETTINGS_FILEPATH);
                return true;
            }
            catch (Exception e) {
                HelperLibrary.Utils.DebugHelper.DebugWriteLine("Loading settings failure: " + e);
                return false;
            }
        }

        private static bool CheckFile() {
            return File.Exists(ConstantSettings.APP_SETTINGS_FILEPATH);
        }

        public static void Save() {
            HelperLibrary.Utils.DebugHelper.DebugWriteLine("Saving settings...");
            try {
                HelperLibrary.Serializers.BinarySerializer<Settings>.SerializeData(ConstantSettings.APP_SETTINGS_FILEPATH,
                    ApplicationSettings);
                HelperLibrary.Utils.DebugHelper.DebugWriteLine("Settings saved successfully.");
            }
            catch (Exception e) {
                string _message = "Saving settings failure: " + e;
                HelperLibrary.Utils.DebugHelper.DebugWriteLine(_message);
                MessageBox.Show(_message, "Settings", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        
        /// <summary>
        /// Load highlight definition to <see cref="HighlightingDefinition"/> property.
        /// </summary>
        private static bool LoadHighlightDefinition() {
            string _path = ConstantSettings.APP_SETTINGS_DIR + "\\HighLight.xshd";
            if (!File.Exists(_path)) return false;
            try {
                HelperLibrary.Utils.DebugHelper.DebugWriteLine("Loading syntax highlighting...");
                using (XmlTextReader reader = new XmlTextReader(ConstantSettings.APP_SETTINGS_DIR + "\\HighLight.xshd")) {
                    ApplicationCommonSettings.HighlightingDefinition = 
                        ICSharpCode.AvalonEdit.Highlighting.Xshd.HighlightingLoader.Load(reader, HighlightingManager.Instance);
                }
                HelperLibrary.Utils.DebugHelper.DebugWriteLine("Syntax loaded successfully.");
                return true;
            }
            catch (Exception e) {
                HelperLibrary.Utils.DebugHelper.DebugWriteLine("Loading syntax highlighting fail: Exception" + e.ToString());
                return false;
            }
        }
        
        /// <summary>
        /// Get matrix configuration from IvisionStudio settings path.
        /// </summary>
        /// <returns>Collection of matrix's.</returns>
        private static IEnumerable<HwMatrixConfiguration> GetMatrixConfigurations() {
            foreach (string file in Directory.GetFiles(ConstantSettings.IVISION_SETTINGS_DIR + "\\HWMatrixConfiguration", "*.hwm", SearchOption.TopDirectoryOnly)) {
                yield return new HwMatrixConfiguration(file);
            }
        }
    }
}