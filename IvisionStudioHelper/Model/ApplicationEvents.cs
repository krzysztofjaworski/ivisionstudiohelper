using System;

namespace IvisionStudioHelper.Model {
    /// <summary>
    /// Class responsible for manage events in application
    /// </summary>
    public static class ApplicationEvents {
        public static event EventHandler RequestCloseProbe;
        private static void OnRequestCloseProbe() {
            RequestCloseProbe?.Invoke(null, EventArgs.Empty);
        }

        public static void SendRequestCloseProbe() {
            OnRequestCloseProbe();
        }

        public static event EventHandler RequestSwitchOffLeds;
        private static void OnRequestSwitchOffLeds() {
            RequestSwitchOffLeds?.Invoke(null, EventArgs.Empty);
        }

        public static void SendRequestSwitchOffLeds() {
            OnRequestSwitchOffLeds();
        }
    }
}