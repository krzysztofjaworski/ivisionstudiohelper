﻿using System;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using System.Xml.Serialization;
using HelperLibrary.Common.Args;
using HelperLibrary.MVVM;

namespace IvisionStudioHelper.Model.Pintable {
    /// <summary>
    /// Class represented IvisionStudio Pintable data.
    /// </summary>
    [Serializable]
    public sealed class PintableData : INotifyPropertyChanged {

        public event EventHandler<bool> IsDirtyChanged;

        private void OnIsDirtyChanged(bool e) {
            IsDirtyChanged?.Invoke(this, e);
        }
        
        
        [XmlIgnore]
        public bool CanBeSaved {
            get { return !PinList.Any(x => x.IsDuplicated); }
        }

        [XmlIgnore]
        public bool IsDirty {
            get { return _isDirty; }
            set {
                _isDirty = value;
                OnIsDirtyChanged(value);
            }
        }

        public string CountFormat { get; set; }
        public DateTime Created { get; set; }
        // ReSharper disable once InconsistentNaming
        public string Created_by { get; set; }
        public string Info { get; set; }
        public DateTime LastChanged { get; set; }
        // ReSharper disable once InconsistentNaming
        public string LastChanged_by { get; set; }
        public int PinRange { get; set; }
        public string Version { get; set; }
        private AsyncObservableCollection<Pin> _pinList;
        private bool _isDirty;

        public AsyncObservableCollection<Pin> PinList {
            get { return _pinList; }
            set {
                _pinList = value;
                if (value != null)
                    value.CollectionChanged += ValueOnCollectionChanged;
            }
        }

        private void ValueOnCollectionChanged(object sender, NotifyCollectionChangedEventArgs e) {
            
            void PinChanged(object s, NewOldEventArgs args) {
                IsDirty = true;
            }
            
            
            switch (e.Action) {
                case NotifyCollectionChangedAction.Add: {
                        foreach (var eNewItem in e.NewItems) {
                            if (eNewItem is Pin _pin) {
                                _pin.PinNameChanged += PinOnPinNameChanged;
                                _pin.PinNameChanged += PinChanged;
                                _pin.CommentChanged += PinChanged;
                                _pin.LedAdrChanged += PinChanged;
                                _pin.TypeChanged += PinChanged;
                            }
                        }
                        break;
                    }
                case NotifyCollectionChangedAction.Remove: {
                        foreach (var eOldItem in e.OldItems) {
                            if (eOldItem is Pin _pin) {
                                _pin.PinNameChanged -= PinOnPinNameChanged;
                                _pin.PinNameChanged -= PinChanged;
                                _pin.CommentChanged -= PinChanged;
                                _pin.LedAdrChanged -= PinChanged;
                                _pin.TypeChanged -= PinChanged;    
                            }
                            
                        }
                        break;
                    }
                case NotifyCollectionChangedAction.Reset: {
                        foreach (var eOldItem in e.OldItems) {
                            if (eOldItem is Pin _pin) {
                                _pin.PinNameChanged -= PinOnPinNameChanged;
                                _pin.PinNameChanged -= PinChanged;
                                _pin.CommentChanged -= PinChanged;
                                _pin.LedAdrChanged -= PinChanged;
                                _pin.TypeChanged -= PinChanged;    
                            }
                        }
                        break;
                    }
            }
        }
        
        /// <summary>
        /// Checking duplicated pins.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PinOnPinNameChanged(object sender, NewOldEventArgs e) {
            CheckDuplicatedPins((Pin) sender, e.OldValue.ToString(), e.NewValue.ToString());
        }

        private void CheckDuplicatedPins(object Pin, string oldValue, string newValue) {
            if (newValue == "") ((Pin) Pin).IsDuplicated = false; // If is it empty cannot be duplicated.
            Pin[] _duplicated = PinList.Where(x => x.PinName == newValue && !string.IsNullOrEmpty(x.PinName)).ToArray();  //Getting duplicated pins.
            if (_duplicated.Length < 2) {    //if elements is less then 2.
                if (_duplicated.Length == 1 && _duplicated[0].IsDuplicated) {
                    _duplicated[0].IsDuplicated = false;
                }

                Pin[] _oldDuplicated = PinList
                    .Where(x => x.PinName == oldValue && !string.IsNullOrEmpty(x.PinName)).ToArray(); // Checking old duplicates.
                if (_oldDuplicated.Length >= 2) return;
                foreach (var pin in _oldDuplicated) {
                    pin.IsDuplicated = false;          // Reset duplication.
                }

                return;
            }

            ;
            foreach (var pin in _duplicated) {
                pin.IsDuplicated = true;
            }
        }

        /// <inheritdoc />
        /// <summary>Occurs when a property value changes.</summary>
#pragma warning disable CS0067
        public event PropertyChangedEventHandler PropertyChanged;
#pragma warning restore CS0067
    }


}