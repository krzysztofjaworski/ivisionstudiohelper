﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HelperLibrary.MVVM;

namespace IvisionStudioHelper.Model.Pintable {
    public class SearchPintableData : PropertyChangedBase {
        public string Find { get; set; }
        public bool LookInPinName { get; set; } = true;
        public bool LookInComment { get; set; } = false;
        public bool MatchCase { get; set; } = true;
        public bool WholePhrase { get; set; } = false;
    }
}
