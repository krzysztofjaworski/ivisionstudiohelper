﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IvisionStudioHelper.Model.Pintable {
    public enum PinType {
        Tp,
        Led,
        ConnectorDetection,
        PowerPin,
        PositionMeasurement,
        IdChip
    }
}
