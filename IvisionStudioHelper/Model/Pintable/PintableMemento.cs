using System.Collections.Generic;
using System.Linq;
using HelperLibrary.MVVM;

namespace IvisionStudioHelper.Model.Pintable {
    public class PintableMemento : PropertyChangedBase {
        private Stack<PintableMementoItem> MementoItems { get; } = new Stack<PintableMementoItem>();
        private Stack<PintableMementoItem> MementoItemsMemory { get; } = new Stack<PintableMementoItem>();
        
        public void AddMementoItem(Pin oldItem, Pin newItem) {
            MementoItemsMemory.Clear();
            MementoItems.Push(new PintableMementoItem(new []{oldItem}, new []{newItem}));
        }
        
        public void AddMementoItem(IEnumerable<Pin> oldItems,IEnumerable<Pin> newItems) {
            MementoItemsMemory.Clear();
            MementoItems.Push(new PintableMementoItem(oldItems, newItems));
        }

        public void UndoLastAction(PintableData pintableData) {
            if (MementoItems.Count == 0) return;
            PintableMementoItem _item = MementoItems.Pop();
            MementoItemsMemory.Push(_item);
            foreach (var oldItem in _item.OldItems) {
                Pin currentPin = pintableData.PinList.First(x => x.System == oldItem.System);
                currentPin.AssignPin(oldItem, true);
            }
        }

        public void RedoLastAction(PintableData pintableData) {
            if (MementoItemsMemory.Count == 0) return;
            PintableMementoItem _item = MementoItemsMemory.Pop();
            MementoItems.Push(_item);
            foreach (var newItem in _item.NewItems) {
                Pin currentPin = pintableData.PinList.First(x => x.System == newItem.System);
                currentPin.AssignPin(newItem, true);
            }
        }
    }
}