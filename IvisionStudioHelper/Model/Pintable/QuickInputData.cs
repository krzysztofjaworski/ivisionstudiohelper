using HelperLibrary.MVVM;

namespace IvisionStudioHelper.Model.Pintable {
    public class QuickInputData : PropertyChangedBase {
        public string PinName { get; set; }
        public string Comment { get; set; }
        public string To { get; set; }
    }
}