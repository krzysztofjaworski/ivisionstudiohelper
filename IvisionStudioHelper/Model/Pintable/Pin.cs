﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using HelperLibrary.Common.Args;
using HelperLibrary.MVVM;

namespace IvisionStudioHelper.Model.Pintable {
    /// <summary>
    /// TODO: Add second property changed event to notify if pintable is dirty or not.
    /// </summary>
    [Serializable]
    public class Pin : INotifyPropertyChanged, ICloneable {
        /// <inheritdoc />
        /// <summary>Occurs when a property value changes.</summary>
#pragma warning disable CS0067
        public event PropertyChangedEventHandler PropertyChanged;
#pragma warning restore CS0067

        /// <summary>
        /// Occurs when property <see cref="PinName"/> value changed.
        /// </summary>
        public event EventHandler<NewOldEventArgs> PinNameChanged;

        /// <summary>
        /// Occurs when property <see cref="Comment"/> value changed.
        /// </summary>
        public event EventHandler<NewOldEventArgs> CommentChanged;

        /// <summary>
        /// Occurs when property <see cref="LedAdr"/> value changed.
        /// </summary>
        public event EventHandler<NewOldEventArgs> LedAdrChanged;

        /// <summary>
        /// Occurs when property <see cref="Type"/> value changed.
        /// </summary>
        public event EventHandler<NewOldEventArgs> TypeChanged;

        public event EventHandler<NewOldEventArgs> PinChanged;


        private bool _canInvokeEventOnPinChanged = true;

        protected virtual void OnPinChanged(object old) {
            if (_canInvokeEventOnPinChanged)
                PinChanged?.Invoke(this, new NewOldEventArgs(old, Clone() as Pin));
        }
        
        protected virtual void OnTypeChanged(NewOldEventArgs e) {
            TypeChanged?.Invoke(this, e);
        }

        protected virtual void OnPinNameChanged(NewOldEventArgs e) {
            PinNameChanged?.Invoke(this, e);
        }

        protected virtual void OnCommentChanged(NewOldEventArgs e) {
            CommentChanged?.Invoke(this, e);
        }

        protected virtual void OnLedAdrChanged(NewOldEventArgs e) {
            LedAdrChanged?.Invoke(this, e);
        }


        [XmlIgnore] public bool IsDuplicated { get; set; }

        public string Comment {
            get { return _comment; }
            set {
                string _oldValue = _comment?.Clone()?.ToString();
                object old = Clone();
                _comment = value;
                OnCommentChanged(new NewOldEventArgs(_oldValue, value));
                OnPinChanged(old);
            }
        }

        public int LedAdr {
            get { return _ledAdr; }
            set {
                int _oldValue = _ledAdr;
                object old = Clone();
                _ledAdr = value;
                OnLedAdrChanged(new NewOldEventArgs(_oldValue, _ledAdr));
                OnPinChanged(old);
            }
        }

        private string _pinName;
        private string _comment;
        private int _ledAdr;
        private PinType _type;

        public string PinName {
            get { return _pinName; }
            set {
                string _oldValue = _pinName?.Clone()?.ToString();
                object old = Clone();
                _pinName = value;
                OnPinNameChanged(new NewOldEventArgs(_oldValue, value));
                OnPinChanged(old);
            }
        }

        private static DisplaySystemType _displaySystemType;

        [XmlIgnore]
        public DisplaySystemType DisplaySystemType {
            get { return _displaySystemType; }
            set {
                _displaySystemType = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("DisplaySystem"));
            }
        }

        [XmlIgnore]
        public string DisplaySystem {
            get { return ConvertDisplayType(_displaySystemType); }
        }

        private string ConvertDisplayType(DisplaySystemType displaySystemType) {
            if (displaySystemType == DisplaySystemType.CardPoint) {
                int _value = (System / 65) + 1;
                int _rest = (System % 64) != 0 ? (System % 64) : 64;
                return _value + "." + _rest;
            }

            return System.ToString();
        }


        public int System { get; set; }

        public PinType Type {
            get { return _type; }
            set {
                PinType _oldValue = _type;
                object old = Clone();
                _type = value;
                OnTypeChanged(new NewOldEventArgs(_oldValue, value));
                OnPinChanged(old);
            }
        }

        public void ResetPin() {
            object old = Clone();
            _canInvokeEventOnPinChanged = false;
            Type = PinType.Tp;
            Comment = string.Empty;
            PinName = string.Empty;
            LedAdr = -1;
            IsDuplicated = false;
            _canInvokeEventOnPinChanged = true;
            OnPinChanged(old);
        }

        public void AssignPin(Pin pin, bool disableNotifications = false) {
            if (pin.System != System) throw new Exception("Invalid pin address. Pin must have the same system point.");
            if (disableNotifications) _canInvokeEventOnPinChanged = false;
            PinName = pin.PinName;
            LedAdr = pin.LedAdr;
            Type = pin.Type;
            Comment = pin.Comment;
            _canInvokeEventOnPinChanged = true;
        }

        /// <summary>Creates a new object that is a copy of the current instance.</summary>
        /// <returns>A new object that is a copy of this instance.</returns>
        public object Clone() {
            return new Pin() {
                IsDuplicated = this.IsDuplicated,
                Comment = this.Comment,
                LedAdr = this.LedAdr,
                PinName = this.PinName,
                System = this.System,
                Type = this.Type
            };
        }
    }
}