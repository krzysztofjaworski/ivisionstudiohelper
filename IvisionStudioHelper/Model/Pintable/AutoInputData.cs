﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HelperLibrary.MVVM;

namespace IvisionStudioHelper.Model.Pintable {
    /// <summary>
    /// Data for autoinput window.
    /// </summary>
    public class AutoInputData : PropertyChangedBase {
        public string Name { get; set; } = "Pin";
        public string Start { get; set; } = "1";
        public string Stop { get; set; } = "64";
        public string Comment { get; set; }
        public bool InsertSpace { get; set; } = true;
    }
}
