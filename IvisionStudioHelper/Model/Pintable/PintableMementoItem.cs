using System.Collections;
using System.Collections.Generic;

namespace IvisionStudioHelper.Model.Pintable {
    public class PintableMementoItem {
        
        public PintableMementoItem(IEnumerable<Pin> oldItems, IEnumerable<Pin> newItems) {
            OldItems = oldItems;
            NewItems = newItems;
        }

        public IEnumerable<Pin> OldItems { get; }
        public IEnumerable<Pin> NewItems { get; }
    }
}