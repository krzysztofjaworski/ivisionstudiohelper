using System.Windows;

namespace IvisionStudioHelper.Model {
    public static class ErrorParser {
        public static void ParseError(string error) {
            if (error == null) return;

            while (error.StartsWith("\n")) {
                error =error.Replace("\n", "");
            }
            
            if (error.StartsWith("\r\n")) error = error.Remove(0, 2);

            if (error == "ERR_CONNECT") {
                error = "Error connection to device. Please check if device is power on and try again";
            }
            
            MessageBox.Show(error, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
        }
    }
}