namespace IvisionStudioHelper.Model.Memento {
    public interface IUndoRedo {
        /// <summary>
        /// Restore the last action.
        /// </summary>
        void Undo();
        /// <summary>
        /// Withdraws restoring last action.
        /// </summary>
        void Redo();
    }
}