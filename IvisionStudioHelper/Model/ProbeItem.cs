using IvisionStudioHelper.Model.Pintable;

namespace IvisionStudioHelper.Model {
    public class ProbeItem {
        public string PinName { get; set; }
        public string System { get; set; }
        public Pin Pin { get; set; }
    }
}