using System;
using System.IO;
using System.Linq;
using ICSharpCode.AvalonEdit.Document;
using ICSharpCode.AvalonEdit.Highlighting;
using IvisionStudioHelper.Model;
using IvisionStudioHelper.Model.Settings;
using System.Threading.Tasks;
using System.Windows;
using HelperLibrary.MVVM;
using IvisionStudioHelper.Model.Project;


namespace IvisionStudioHelper.ViewModels {
    public class ProjectViewModel : PropertyChangedBase{
        public string Title { get; set; }
        public bool IsSelected { get; set; }
        private bool _isDirty;

        public bool IsDirty {
            get { return _isDirty || PintableViewModel?.Pintable?.IsDirty == true; }
            private set { _isDirty = value; }
        }

        public TextDocument Text { get; set; } = new TextDocument();
        public PintableViewModel PintableViewModel { get; private set; }
        public IHighlightingDefinition XSD => ApplicationCommonSettings.HighlightingDefinition;
        public Project Project { get; }

        public bool Save() {
            try {
                PintableViewModel.SavePintableAsync();
                if (Project.HasOwnTSL)
                    Application.Current.Dispatcher.Invoke(() => File.WriteAllText(Project.TCLPath, Text.Text));

                Title = Title.Replace("*", "");
                IsDirty = false;
                return true;
            }
            catch (Exception e) {
                MessageBox.Show("File cannot be saved. " + e.ToString(), "Error", MessageBoxButton.OK,
                    MessageBoxImage.Error);
                return false;
            }
        }

        public Task<bool> SaveAsync() {
            return Task.Factory.StartNew<bool>(Save);
        }

        public ProjectViewModel(Project project) {
            Project = project;
            Title = project.Name;
            if (project.HasOwnTSL)
                Text = new TextDocument() {
                    Text = File.ReadAllText(Project.TCLPath)
                };
            InitializePintable(project);
            Text.TextChanged += TextChanged;
        }

        private void InitializePintable(Project project) {
            if (project.HasOwnPintable) {
                bool _itIsSystemPintable = project.Name.ToLower() == "sp";
                PintableViewModel = new PintableViewModel(project.PintablePath, _itIsSystemPintable, !_itIsSystemPintable,Project);
                PintableViewModel.LoadPintableAsync();
                return;
            }

            string _file = Directory.GetFiles(Path.GetDirectoryName(project.Path) ?? throw new InvalidOperationException(), "*.pintable")
                .SingleOrDefault(x => Path.GetFileNameWithoutExtension(x).ToLower() == "sp");

            if (!string.IsNullOrEmpty(_file)) {
                PintableViewModel = new PintableViewModel(_file, true,true ,Project);
            }
            else {
                return;
            }
            
            PintableViewModel.LoadPintableAsync();
        }

        private void TextChanged(object sender, EventArgs e) {
            if (!IsDirty) {
                IsDirty = true;
                Title += "*";
            }
        }
    }
    
}