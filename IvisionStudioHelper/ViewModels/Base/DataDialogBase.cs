using System.Windows.Controls;
using System.Windows.Input;
using HelperLibrary.MVVM;

namespace IvisionStudioHelper.ViewModels.Base {
    public abstract class DataDialogBase<T> : DialogBase, IDataDialog<T> {
        /// <summary>
        /// Data represented in View.
        /// </summary>
        public T Data { get; protected set; }

        /// <summary>
        /// Constructor.
        /// </summary>
        protected DataDialogBase() {
            LoadDataFromSettings();
        }

        /// <summary>
        /// Loading data from settings.
        /// </summary>
        protected abstract void LoadDataFromSettings();

        public bool IsCapsLockLock { get; private set; }
    
        public ICommand KeyDownCommand => new RelayCommand<object>(KeyDownCommandMethod);

        public void KeyDownCommandMethod(object obj) {
            IsCapsLockLock = Keyboard.IsKeyToggled(Key.CapsLock);
        }
    }
}