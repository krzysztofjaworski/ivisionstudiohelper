﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HelperLibrary.MVVM;

namespace IvisionStudioHelper.ViewModels.Base
{
    public class DialogBase : PropertyChangedBase
    {
        public bool Dialog { get; set; }
    }
}
