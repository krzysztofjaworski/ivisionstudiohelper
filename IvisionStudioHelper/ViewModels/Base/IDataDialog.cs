namespace IvisionStudioHelper.ViewModels.Base {
    public interface IDataDialog<T> {
        T Data { get; }
    }
}