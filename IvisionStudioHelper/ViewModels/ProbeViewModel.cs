using System;
using System.Collections.Generic;
using System.Linq;
using System.Media;
using System.Windows.Forms;
using HelperLibrary.MVVM;
using IvisionStudioHelper.Model;
using IvisionStudioHelper.Model.Pintable;
using IvisionStudioHelper.Model.Settings;
using IvisionStudioHelper.ViewModels.Base;

namespace IvisionStudioHelper.ViewModels {
    public sealed class ProbeViewModel : DialogBase, IWindowService {
        private ProbeItem _firstPin;

        public ProbeItem FirstPin {
            get { return _firstPin; }
            private set {
                _firstPin = value;
                OnProbePinDetect(value.Pin);
                if (System.IO.File.Exists(AppDomain.CurrentDomain.BaseDirectory + "ProbeDing.wav"))
                    _player.Play();
            }
        }

        readonly SoundPlayer _player = new SoundPlayer(AppDomain.CurrentDomain.BaseDirectory + "ProbeDing.wav");
        public AsyncObservableCollection<ProbeItem> Pins { get; } = new AsyncObservableCollection<ProbeItem>();
        public ProjectViewModel Project { get; }
        public event EventHandler<Pin> ProbePinDetect;
        public string Status { get; set; }

        private void OnProbePinDetect(Pin e) {
            ProbePinDetect?.Invoke(this, e);
        }

        public ProbeViewModel(ProjectViewModel project) {
            Status = "Initializing...";
            Project = project;
            ApplicationCommonSettings.DeviceManager.OnError += (sender, s) => this.CloseMethod();
        }

        public void StartProbe() {
            ApplicationCommonSettings.DeviceManager.StartProbe(OnDataReceived, Project.Project.Path);
            Status = "Probe Active";
            PlaySoundConnected();
        }

        private static void PlaySoundConnected() {
            string path = AppDomain.CurrentDomain.BaseDirectory + "Connected.wav";
            if (!System.IO.File.Exists(path)) return;
            SoundPlayer _connectedPlayer = new SoundPlayer(AppDomain.CurrentDomain.BaseDirectory + "Connected.wav");
            _connectedPlayer.Play();
        }

        private void OnDataReceived(IEnumerable<int> pts) {
            Pins.Clear();
            var arrayPins = pts as int[] ?? pts?.ToArray();
            if (arrayPins == null) return;

            if (arrayPins.Length >= 1) {
                FirstPin = GetProbeItem(arrayPins[0]);
            }

            if (arrayPins.Length <= 1) return;

            foreach (int pt in arrayPins.Skip(1)) {
                Pin pin = Project.PintableViewModel.Pintable.PinList.SingleOrDefault(x => x.System == pt);
                if (pin == null) continue;
                if (!Pins.Contains(FirstPin))
                    Pins.Add(GetProbeItem(pt));
            }
        }

        private ProbeItem GetProbeItem(int pt) {
            Pin _pin = Project.PintableViewModel.Pintable.PinList.Single(x => x.System == pt);
            ProbeItem _probeItem = new ProbeItem() {
                PinName = _pin.PinName,
                System = _pin.DisplaySystem,
                Pin = _pin
            };

            return _probeItem;
        }

        public DialogResult DialogResult { get; set; }
        public Action CloseMethod { get; set; }

        public void CloseEvent() {
            ApplicationCommonSettings.DeviceManager.StopProbe();
        }
    }
}