﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HelperLibrary.Extensions;
using IvisionStudioHelper.Model.Pintable;
using IvisionStudioHelper.Model.Settings;
using IvisionStudioHelper.ViewModels.Base;

namespace IvisionStudioHelper.ViewModels {
    public class QuickInputViewModel : DataDialogBase<QuickInputData> {

        public QuickInputViewModel(Pin selectedPin) {
            Data.Comment = selectedPin?.Comment;
            if (!string.IsNullOrEmpty(selectedPin?.PinName)) {
                int index = selectedPin.PinName.IndexOf(' ');
                Data.PinName = index == -1 ? selectedPin.PinName : selectedPin.PinName.Substring(0, index);
            }
            else {
                Data.PinName = "";
            }
        }
        protected override void LoadDataFromSettings() {
            Data = TemporarySettings.LastQuickInputData == null ? new QuickInputData() : TemporarySettings.LastQuickInputData;
        }
    }
}
