using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using CustomControls;
using HelperLibrary.Common.Args;
using HelperLibrary.Extensions;
using HelperLibrary.MVVM;
using IvisionStudioHelper.Model;
using IvisionStudioHelper.Model.Memento;
using IvisionStudioHelper.Model.Pintable;
using IvisionStudioHelper.Model.Project;
using IvisionStudioHelper.Model.Settings;
using WK260;

namespace IvisionStudioHelper.ViewModels {

    /// <summary>
    /// View model for pintable view.
    /// </summary>
    public sealed class PintableViewModel : PropertyChangedBase, IUndoRedo {
        private PintableMemento Memento { get;} = new PintableMemento();
        /// <summary>
        /// Title of pintable in above datagrid.
        /// </summary>
        public string Title { get; set; }
        
        /// <summary>
        /// All data of items in pintable.
        /// </summary>
        public PintableData Pintable { get; private set; }
        /// <summary>
        /// It is pintable.
        /// </summary>
        public bool IsItSystemPintable { get; }
        /// <summary>
        /// Property responsible for checking if pintable is readonly.
        /// </summary>
        public bool IsReadOnly { get; }
        public bool ErrorOnLoading { get; private set; }
        
        /// <summary>
        /// Selected items in pintable.
        /// </summary>
        public AsyncObservableCollection<Pin> SelectedItems { get; set; } = new AsyncObservableCollection<Pin>();
        
        private Pin _selectedItem;
        /// <summary>
        /// Selected current pin in pintable.
        /// </summary>
        public Pin SelectedItem {
            get { return _selectedItem; }
            set {
                _selectedItem = value;
                if (GlowLed)
                    LedOnCommandMethod(null);
            }
        }
        /// <summary>
        /// Keep list of copied or cut pins.
        /// </summary>
        public AsyncObservableCollection<Pin> MemoryPinsAddresses { get; } = new AsyncObservableCollection<Pin>();

        private bool _glowLed;
        /// <summary>
        /// Toggle to glow led when selected pin is selected.
        /// </summary>
        public bool GlowLed {
            get { return _glowLed; }
            set {
                ErrorOnLoading = false;            // Resetting flag.
                if (value && !ApplicationCommonSettings.DeviceManager.IsConnected) {
                    MessageBox.Show("Device is offline.", "Offline", MessageBoxButton.OK, MessageBoxImage.Warning);
                    _glowLed = false;
                    return;
                }
                if (value) {
                    if (ApplicationCommonSettings.DeviceManager.IsProbeActive)
                        ApplicationEvents.SendRequestCloseProbe(); //Sending request to close probe window if is open.
                    LedOnCommandMethod(null);
                } else {
                    ApplicationCommonSettings.DeviceManager.ResetLeds();
                }

                _glowLed = !ErrorOnLoading && value;
            }
        }
        
        /// <summary>
        /// Occurs when user requesting autoinput from upper menu or context menu.
        /// </summary>
        public event EventHandler AutoInputRequestEvent;
        /// <summary>
        /// Occurs when user requesting pintable search from upper menu or context menu.
        /// </summary>
        public event EventHandler SearchPintableRequestEvent;
        /// <summary>
        /// Occurs when user requesting quick input context menu.
        /// </summary>
        public event EventHandler QuickInputRequestEvent;
        
        private void OnAutoInputRequest() {
            AutoInputRequestEvent?.Invoke(this, EventArgs.Empty);
        }
        
        private void OnSearchPintableRequestEvent() {
            SearchPintableRequestEvent?.Invoke(this, EventArgs.Empty);
        }

        public void OnQuickInputRequestEvent() {
            QuickInputRequestEvent?.Invoke(this, EventArgs.Empty);
        }

        /// <summary>
        /// Path to pintable.
        /// </summary>
        private readonly string _path;
        /// <summary>
        /// 
        /// </summary>
        public Project Project { get; }

        /// <summary>
        /// Constructor to create view model for pintable view.
        /// </summary>
        /// <param name="path">Path to pintable.</param>
        /// <param name="isItSystemPintable">Is it system pintable or local pintable assigned to project.</param>
        /// <param name="isReadOnly">Flag to set pintable as readonly for user interactivity.</param>
        /// <param name="project">Project which one pintable is associated.</param>
        public PintableViewModel(string path, bool isItSystemPintable, bool isReadOnly, Project project) {
            _path = path;
            IsItSystemPintable = isItSystemPintable;
            SetTitle(IsItSystemPintable);
            IsReadOnly = isReadOnly;
            Project = project;
            ApplicationCommonSettings.DeviceManager.OnError += DeviceManagerOnOnError;
            ApplicationEvents.RequestSwitchOffLeds += ApplicationEventsOnRequestSwitchOffLeds;
        }

        private void AddSubscriberToPins() {
            foreach (var pin in Pintable.PinList) {
                pin.PinChanged += PinOnPinChanged;
            }
        }
        
        private void RemoveSubscriberToPins() {
            foreach (var pin in Pintable.PinList) {
                pin.PinChanged -= PinOnPinChanged;
            }
        }

        private void PinOnPinChanged(object sender, NewOldEventArgs e) {
            Memento.AddMementoItem(e.OldValue as Pin, e.NewValue as Pin);
        }

        private void SetTitle(bool isItSystemPintable) {
            Title = (isItSystemPintable) ? "System pintable" : "Pintable";
        }

        private void ApplicationEventsOnRequestSwitchOffLeds(object sender, EventArgs e) {
            if (GlowLed) {
                GlowLed = false;
            }
        }

        private void DeviceManagerOnOnError(object sender, string e) {
            ErrorOnLoading = true;
            //ErrorParser.ParseError(e);
        }

        /// <summary>
        /// Initialize pintable.
        /// </summary>
        public void LoadPintable() {
            try {
                Pintable = HelperLibrary.Serializers.XmlSerializerHelper<PintableData>.DeserializeData(_path);
                Pintable.IsDirtyChanged += PintableOnIsDirtyChanged;
                AddSubscriberToPins();
            } catch (Exception e) {
                MessageBox.Show("Error loading pintable " + e);
                throw;
            }
        }

        private void PintableOnIsDirtyChanged(object sender, bool e) {
            if (e) Title += "*";
            else Title = Title.Replace("*", "");
        }

        /// <summary>
        /// Initialize async pintable.
        /// </summary>
        /// <returns></returns>
        public Task LoadPintableAsync() {
            return Task.Factory.StartNew(LoadPintable);
        }

        /// <summary>
        /// Save pintable to existing file.
        /// </summary>
        public bool SavePintable() {
            try {
                if (IsReadOnly) {
                    Pintable.IsDirty = false;
                    return true;
                }
                if (Pintable.CanBeSaved) {
                    HelperLibrary.Serializers.XmlSerializerHelper<PintableData>.SerializeData(_path, Pintable);
                    Pintable.IsDirty = false;
                    return true;
                }
                MessageBox.Show("Duplicated pins in pintable. Please resolve problem.", "Duplicated pins.", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            } catch (Exception e) {
                MessageBox.Show("Error saving pintable " + e);
                return false;
            }
        }

        /// <summary>
        /// Save pintable async to existing file.
        /// </summary>
        /// <returns></returns>
        public Task<bool> SavePintableAsync() {
            return Task.Factory.StartNew<bool>(SavePintable);
        }

        /// <summary>
        /// Request to input automatically data to pintable.
        /// </summary>
        /// <param name="data">Pintable data,</param>
        public void AutoInputRequestChanged(AutoInputData data) {
            if ((int.TryParse(data.Start, out int _start) && int.TryParse(data.Stop, out int _stop))) {
                InputIntegerData(_start, _stop, data);
            }
        }

        /// <summary>
        /// Request to search pintable.
        /// </summary>
        /// <param name="data">Searching pintable data.</param>
        public void SearchPintableRequest(SearchPintableData data) {
            bool Compare(string searched, string phrase) {
                if (data.WholePhrase) {
                    return (data.MatchCase)
                        ? string.Compare(searched, phrase, StringComparison.Ordinal) == 0
                        : string.Compare(searched, phrase, StringComparison.OrdinalIgnoreCase) == 0;
                }
                
                return (data.MatchCase)
                    ? phrase.Contains(searched)
                    : phrase.ToLower().Contains(searched.ToLower());
            }

            if (string.IsNullOrEmpty(data.Find)) return;
            int _start = SelectedItem != null ? Pintable.PinList.IndexOf(SelectedItem) : 0;

            bool Search(int index, int stop) {
                for (int i = index; i < stop; i++) {
                    Pin _pin = Pintable.PinList[i];
                    if (data.LookInPinName) {
                        if (Compare(data.Find, _pin.PinName)) {
                            SelectedItem = _pin;
                            return true;
                        }
                    }

                    if (!data.LookInComment) continue;
                    if (!Compare(data.Find, _pin.Comment)) continue;
                    SelectedItem = _pin;
                    return true;
                }
                return false;
            }

            if (Search(_start, Pintable.PinList.Count) || _start <= 0) return;
            if (!Search(0, _start))
                MessageBox.Show("Element not found.", "Searching result.", MessageBoxButton.OK,
                    MessageBoxImage.Information);
        }

        /// <summary>
        /// Request for quick input data to pintable.
        /// </summary>
        /// <param name="data">Data to be input.</param>
        public void QuickInputRequest(QuickInputData data) {
            if (SelectedItem == null || !int.TryParse(data.To, out int to)) return;
            InputIntegerData(1, to, new AutoInputData() {
                Name = data.PinName,
                Stop = data.To,
                Start = "1",
                Comment = "",
                InsertSpace = true
            });
        }

        /// <summary>
        /// Find first described pin from selected one. Searching up.
        /// </summary>
        /// <returns>Described pin.</returns>
        public Pin FindFirstDescribedPinFromSelected() {
            if (SelectedItem == null) return null;
            for (int i = SelectedItem.System; i >= 0; i--) {
                Pin _pin = Pintable.PinList[i];
                if (!string.IsNullOrEmpty(_pin.PinName)) return _pin;
            }

            return null;
        }

        /// <summary>
        /// Inputting automatically data to pintable. 
        /// </summary>
        /// <param name="start">From pin</param>
        /// <param name="stop">To pin</param>
        /// <param name="data">Pintable data</param>
        private void InputIntegerData(int start, int stop, AutoInputData data) {
            RemoveSubscriberToPins();
            int _system = SelectedItem.System;
            int flag = 0;
            
            List<Pin> oldPins = new List<Pin>();
            List<Pin> newPins = new List<Pin>();
            for (int i = start; i < stop + 1; i++) {
                Pin _pin = Pintable.PinList.FirstOrDefault(x => x.System == _system + flag);
                flag++;
                if (_pin == null) return;        // If pin == null return. No need more actions to be executed.
                oldPins.Add(_pin?.Clone() as Pin);
                _pin.PinName = data.Name + ((data.InsertSpace) ? " " : "") + i.ToInt32();    //Inserting space automatically if need and set PinName.
                _pin.Comment = data.Comment;                                                 //Setting comment.
                newPins.Add(_pin.Clone() as Pin);
            }
            Memento.AddMementoItem(oldPins, newPins);
            AddSubscriberToPins();
        }

        #region Commands

        /// <summary>
        /// Command responsible for invoke quick input to pintable dialog.
        /// </summary>
        public ICommand QuickInputCommand => new RelayCommand(QuickInputCommandMethod);
        /// <summary>
        /// Method for <see cref="QuickInputCommand"/> command.
        /// </summary>
        /// <param name="obj">Should be null.</param>
        private void QuickInputCommandMethod(object obj) {
            OnQuickInputRequestEvent();
        }
        
        /// <summary>
        /// Command responsible for invoke searching pintable dialog.
        /// </summary>
        public ICommand SearchPintableCommand => new RelayCommand(SearchPintableCommandMethod);
        /// <summary>
        /// Method for <see cref="SearchPintableCommand"/> command.
        /// </summary>
        /// <param name="obj">Should be null</param>
        private void SearchPintableCommandMethod(object obj) {
            OnSearchPintableRequestEvent();
        }
        
        /// <summary>
        /// Command responsible for request to show autoinput windows.
        /// </summary>
        public ICommand AutoInputCommand => new RelayCommand(AutoInputCommandMethod, (o) => SelectedItem != null);

        /// <summary>
        /// Method for <see cref="AutoInputCommand"/> command.
        /// </summary>
        /// <param name="obj">Should be null.</param>
        private void AutoInputCommandMethod(object obj) {
            OnAutoInputRequest();
        }

        /// <summary>
        /// Command for deleting pins from pintable.
        /// </summary>
        public ICommand DeletePinsCommand => new RelayCommand(DeletePinsCommandMethod, (o) => SelectedItems != null && SelectedItems.Count > 0);

        /// <summary>
        /// Method for command <see cref="DeletePinsCommand"/> deleting pins from pintable.
        /// </summary>
        /// <param name="obj">Should be null.</param>
        private void DeletePinsCommandMethod(object obj) {
            foreach (var selectedItem in SelectedItems) {
                selectedItem.ResetPin();
            }
        }
        /// <summary>
        /// Turn on leds on selected pin if is it led.
        /// </summary>
        public ICommand LedOnCommand => new RelayCommand(LedOnCommandMethod);
        private void LedOnCommandMethod(object obj) {
            if (SelectedItem == null) return;
            if (SelectedItem.Type != PinType.Led) {
                if (!ApplicationCommonSettings.DeviceManager.IsConnected) return;
                if (!_glowLed) ApplicationCommonSettings.DeviceManager.InitLeds(Project.Range);
                ResetLeds();
                return;
            }
            if (!ApplicationCommonSettings.DeviceManager.IsConnected) return;
            try {
                ResetLeds();
                if (!_glowLed) ApplicationCommonSettings.DeviceManager.InitLeds(Project.Range);
                ApplicationCommonSettings.DeviceManager.SetLedOn(SelectedItem.System);
            } catch (DeviceOperationException e) {
                HelperLibrary.Utils.DebugHelper.DebugWriteLine(e.ToString());
            }
        }
        /// <summary>
        /// Turing off all leds.
        /// </summary>
        private void ResetLeds() {
            try {
                if (!ApplicationCommonSettings.DeviceManager.IsConnected) return;
                ApplicationCommonSettings.DeviceManager.ResetLeds();
            } catch (DeviceOperationException e) {
                HelperLibrary.Utils.DebugHelper.DebugWriteLine(e.ToString());
            }
        }
        /// <summary>
        /// Command responsible for copy elements to memory.
        /// </summary>
        public ICommand CopySelectedPinsCommand => new RelayCommand(CopySelectedPinsCommandMethod);
        public void CopySelectedPinsCommandMethod(object obj) { 
            MemoryPinsAddresses.Clear();
            foreach (var pin in SelectedItems.OrderBy(x => x.System)) {
                MemoryPinsAddresses.Add(pin.Clone() as Pin);
            }
        }
        /// <summary>
        /// Command responsible for cut elements from pintable and copy it to memory.
        /// </summary>
        public ICommand CutSelectedPinsCommand => new RelayCommand(CutSelectedPinsCommandMethod);
        public void CutSelectedPinsCommandMethod(object obj) {
            RemoveSubscriberToPins();
            CopySelectedPinsCommandMethod(null);
            List<Pin> oldItems = new List<Pin>();
            List<Pin> newItems = new List<Pin>();
            foreach (var pin in SelectedItems) {
                oldItems.Add(pin.Clone() as Pin);
                pin.ResetPin();
                newItems.Add(pin.Clone() as Pin);
            }
            Memento.AddMementoItem(oldItems, newItems);
            AddSubscriberToPins();
        }
        /// <summary>
        /// Command responsible for pasting elements from memory to pintable.
        /// </summary>
        public ICommand PasteSelectedPinsCommand => new RelayCommand(PasteSelectedPinsCommandMethod);
        public void PasteSelectedPinsCommandMethod(object obj) {
            if (SelectedItem == null) return;
            List<Pin> oldItems = new List<Pin>();
            List<Pin> newItems = new List<Pin>();
            for (int i = 0; i < MemoryPinsAddresses.Count; i++) {
                if (SelectedItem.System + i > Pintable.PinList.Count) break; 
                Pin _memoryPin = MemoryPinsAddresses[i];
                Pin _pintablePin = Pintable.PinList[SelectedItem.System + i - 1];
                oldItems.Add(_pintablePin.Clone() as Pin);
                AssignPin(_pintablePin, _memoryPin);
                newItems.Add(_pintablePin.Clone() as Pin);
            }
            Memento.AddMementoItem(oldItems, newItems);
            MemoryPinsAddresses.Clear();
        }
        /// <summary>
        /// Assign pin from memory to existing pin in pintable.
        /// </summary>
        /// <param name="pintablePin">Existing pin object.</param>
        /// <param name="memoryPin">Pin object from memory.</param>
        private void AssignPin(Pin pintablePin, Pin memoryPin) {
            RemoveSubscriberToPins();
            pintablePin.Type = memoryPin.Type;
            pintablePin.Comment = memoryPin.Comment;
            pintablePin.IsDuplicated = memoryPin.IsDuplicated;
            pintablePin.LedAdr = memoryPin.LedAdr;
            pintablePin.PinName = memoryPin.PinName;
            AddSubscriberToPins();
        }

        #endregion

        #region Undo Redo Commands

        public ICommand UndoCommand => new RelayCommand<object>(UndoCommandMethod);

        public void UndoCommandMethod(object obj) {
            Undo();
        }

        public void Undo() {
            Memento.UndoLastAction(Pintable);
        }
        
        public ICommand RedoCommand => new RelayCommand<object>(RedoCommandMethod);

        public void RedoCommandMethod(object obj) {
            Redo();
        }

        public void Redo() {
            Memento.RedoLastAction(Pintable);
        }

        #endregion
    }
}
