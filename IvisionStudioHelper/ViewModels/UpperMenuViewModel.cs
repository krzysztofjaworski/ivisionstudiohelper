using System;
using System.Windows.Input;
using HelperLibrary.MVVM;
using IvisionStudioHelper.ViewModels.Commands;
using Microsoft.WindowsAPICodePack.Dialogs;
using WK260;

namespace IvisionStudioHelper.ViewModels {
    /// <summary>
    /// View model responsible for upper menu.
    /// </summary>
    public class UpperMenuViewModel : PropertyChangedBase {
        public TesterInformation TesterInformation { get; set; }
        /// <summary>
        /// Open folder command.
        /// </summary>
        public ICommand OpenFolderCommand => UpperMenuCommands.OpenFolderCommand;
        /// <summary>
        /// Command for create new project.
        /// </summary>
        public ICommand NewProjectCommand => UpperMenuCommands.NewProjectCommand;
        /// <summary>
        /// Save selected project.
        /// </summary>
        public ICommand SaveCommand => UpperMenuCommands.SaveCommand;
        /// <summary>
        /// Open probe window.
        /// </summary>
        public ICommand ProbeCommand => UpperMenuCommands.ProbeCommand;
        /// <summary>
        /// Save all opened project. 
        /// </summary>
        public ICommand SaveAllCommand => UpperMenuCommands.SaveAllCommand;
        /// <summary>
        /// Compile opened project.
        /// </summary>
        public ICommand CompileProject => UpperMenuCommands.CompileCommand;

    }
}