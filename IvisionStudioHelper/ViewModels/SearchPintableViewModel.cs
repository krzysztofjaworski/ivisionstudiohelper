using HelperLibrary.MVVM;
using IvisionStudioHelper.Model.Pintable;
using IvisionStudioHelper.Model.Settings;
using IvisionStudioHelper.ViewModels.Base;

namespace IvisionStudioHelper.ViewModels {
    public class SearchPintableViewModel : DataDialogBase<SearchPintableData> {
        protected override void LoadDataFromSettings() {
            Data = TemporarySettings.LastSearchedPintableData == null ? new SearchPintableData() : TemporarySettings.LastSearchedPintableData;
        }
    }
}