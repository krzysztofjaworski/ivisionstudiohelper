using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows.Controls;
using System.Windows.Input;
using HelperLibrary.MVVM;
using IvisionStudioHelper.Model;
using IvisionStudioHelper.Model.Project;
using IvisionStudioHelper.Model.Settings;

namespace IvisionStudioHelper.ViewModels {
    public class NewProjectViewModel : ViewModelBase {
#pragma warning disable 414
        private bool _isNew;
#pragma warning restore 414
        public Project Project { get; set; }
        public string ProjectName { get; set; }
        public string Description { get; set; }
        public string LoggedUser { get; set; }
        public int Range { get; set; }
        public IEnumerable<HwMatrixConfiguration> MatrixConfigurations { get; }
        public HwMatrixConfiguration SelectedMatrixConfiguration { get; set; }
        public bool CreateEmptyPintable { get; set; }
        public bool CreateParameterFile { get; set; }
        public bool UseDefaultConfiguration { get; set; } = true;

        public NewProjectViewModel(int defaultRange) {
            _isNew = true;
            LoggedUser = System.Environment.UserName;
            Range = defaultRange;
            MatrixConfigurations = ApplicationCommonSettings.MatrixConfigurations;
            IEnumerable<HwMatrixConfiguration> hwMatrixConfigurations = MatrixConfigurations as HwMatrixConfiguration[] ?? MatrixConfigurations.ToArray();            
        }

        public NewProjectViewModel(Project project) {
            _isNew = false;
            Project = project;
            ProjectName = project.Name;
            Description = project.Description;
            IEnumerable<HwMatrixConfiguration> hwMatrixConfigurations = MatrixConfigurations as HwMatrixConfiguration[] ?? MatrixConfigurations.ToArray();
            Range = project.Range;
            SelectedMatrixConfiguration = MatrixConfigurations.SingleOrDefault(x => x.Name == Project.HwMatrixConfigurationName);

        }

        //Commands
        public ICommand OkCommand => new RelayCommand<object>(OkCommandMethod, (o => !string.IsNullOrEmpty(ProjectName) && Range > 0));
        private void OkCommandMethod(object obj) {
            
        }

        public ICommand CancelCommand => new RelayCommand<object>(CancelCommandMethod);
        private void CancelCommandMethod(object obj) {
            
        }
    }

    public class RangeValidation : ValidationRule {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo) {
            if (value == null)
                return new ValidationResult(false, "value cannot be empty.");
            else
            {
                if (value.ToString().Length > 3)
                    return new ValidationResult
                        (false, "Name cannot be more than 3 characters long.");
            }
            return ValidationResult.ValidResult;
        }
    }
}