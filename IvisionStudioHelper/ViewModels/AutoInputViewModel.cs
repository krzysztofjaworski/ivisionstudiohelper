﻿using System.Windows.Input;
using HelperLibrary.MVVM;
using IvisionStudioHelper.Model.Pintable;
using IvisionStudioHelper.Model.Settings;
using IvisionStudioHelper.ViewModels.Base;

namespace IvisionStudioHelper.ViewModels {
    /// <summary>
    /// View model for window Autoinput.
    /// </summary>
    public class AutoInputViewModel : DataDialogBase<AutoInputData> {
        /// <summary>
        /// Loading data from settings.
        /// </summary>
        protected override void LoadDataFromSettings() {
            Data = TemporarySettings.LastAutoInputData == null ? new AutoInputData() : TemporarySettings.LastAutoInputData;
        }
        
    }


}