using System;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using CustomControls;
using HelperLibrary.MVVM;
using IvisionStudioHelper.Model;
using IvisionStudioHelper.Model.Pintable;
using IvisionStudioHelper.Model.Project;
using IvisionStudioHelper.Model.Settings;
using IvisionStudioHelper.ViewModels.Base;
using IvisionStudioHelper.ViewModels.Commands;
using IvisionStudioHelper.Views;
using Microsoft.WindowsAPICodePack.Dialogs;
using WK260;

namespace IvisionStudioHelper.ViewModels {
    public class MainViewModel : PropertyChangedBase {
        public TesterInformation TesterInformation {
            get { return UpperMenu.TesterInformation; }
            set { UpperMenu.TesterInformation = value; }
        }

        public ProjectExplorerViewModel ProjectExplorerViewModel { get; set; } = new ProjectExplorerViewModel();
        public UpperMenuViewModel UpperMenu { get; } = new UpperMenuViewModel();
        public AsyncObservableCollection<ProjectViewModel> OpenedProjects { get; set; } = new AsyncObservableCollection<ProjectViewModel>();
        public ProjectViewModel SelectedProject { get; set; }

        public ICommand AutoInputRequestCommand => new RelayCommand(AutoInputRequestCommandMethod);
        private void AutoInputRequestCommandMethod(object obj) {
            if (SelectedProject?.PintableViewModel?.AutoInputCommand.CanExecute(null) == true) {
                SelectedProject?.PintableViewModel?.AutoInputCommand.Execute(null);
            }
        }

        private void PintableViewModelOnAutoInputRequest(object sender, EventArgs e) {
            OpenDataDialogWindow(AutoInputWindowSettings,
                typeof(AutoInputViewModel), (AutoInputData o) => TemporarySettings.LastAutoInputData = o, SelectedProject.PintableViewModel.AutoInputRequestChanged);
        }

        public MainViewModel() {
            InitializeDeviceManager();
            SubscribeUpperMenuEvents();
            SubscribeProjectExplorerEvents();
            SubscribeApplicationEvents();
            CheckLastOpenedPath();
        }

        public void SubscribeApplicationEvents() {
            ApplicationEvents.RequestCloseProbe += (sender, args) => {
                if (ApplicationCommonSettings.DeviceManager.IsProbeActive && ProbeWindowSettings?.IsOpen == true) {
                    (ProbeWindowSettings.DataContext as ProbeViewModel)?.CloseMethod();
                }
            };
        }

        private void InitializeDeviceManager() {
            ApplicationCommonSettings.DeviceManager = new DeviceManager(ConstantSettings.IVISION_SYSTEM_PARAMETER_FILEPATH);
            ApplicationCommonSettings.DeviceManager.ConnectionStateChanged += ManagerOnConnectionStateChanged;
            ApplicationCommonSettings.DeviceManager.OnError += DeviceManagerOnOnError;
            ApplicationCommonSettings.DeviceManager.ConnectDevice();
        }

        private void DeviceManagerOnOnError(object sender, string message) {
            if (!string.IsNullOrEmpty(message)) {
                ErrorParser.ParseError(message);
            }
        }

        private void ManagerOnConnectionStateChanged(object sender, TesterInformation e) {
            HelperLibrary.Utils.DebugHelper.DebugWriteLine("Getting information from tester...");
            try {
                TesterInformation = e;
#if (DEBUG)
                if (TesterInformation != null) {
                    HelperLibrary.Utils.DebugHelper.DebugWriteLine("Information feted successfully from device.");
                    HelperLibrary.Utils.DebugHelper.DebugWriteLine($"Firmware version: {TesterInformation.FirmwareVersion}");
                    HelperLibrary.Utils.DebugHelper.DebugWriteLine($"FPGA version: {TesterInformation.FPGAVersion}");
                    HelperLibrary.Utils.DebugHelper.DebugWriteLine($"Serial number: {TesterInformation.SrnNumber}");
                } else {
                    HelperLibrary.Utils.DebugHelper.DebugWriteLine("Information feted fail from device.");
                }
#endif                         
            } catch (DeviceOperationException) {
                TesterInformation = null;
            }
        }

        private void CheckLastOpenedPath() {
            if (!string.IsNullOrEmpty(ConfigurationManager.ApplicationSettings.LastOpenedPath) && 
                Directory.Exists(ConfigurationManager.ApplicationSettings.LastOpenedPath)) 
                ProjectExplorerViewModel.Initialize(ConfigurationManager.ApplicationSettings.LastOpenedPath);
        }

        private void SubscribeProjectExplorerEvents() {
            ProjectExplorerViewModel.ProjectIsSelected += ProjectExplorer_OnSelectItem;
        }

        private void ProjectExplorer_OnSelectItem(object sender, Project e) {

            //Prevent to open two the same items.
            var _openedProject = CheckIfProjectIsAlreadyOpened(e);
            if (_openedProject != null) {
                SelectedProject = _openedProject;
                return;
            }


            ProjectViewModel _viewModel = new ProjectViewModel(e);
            OpenedProjects.Add(_viewModel);
            SelectedProject = _viewModel;
            _viewModel.PintableViewModel.AutoInputRequestEvent += PintableViewModelOnAutoInputRequestEvent;
            _viewModel.PintableViewModel.SearchPintableRequestEvent += PintableViewModelOnSearchPintableRequestEvent;
            _viewModel.PintableViewModel.QuickInputRequestEvent += PintableViewModelOnQuickInputRequestEvent;
            _viewModel.IsSelected = true;
        }

        private void PintableViewModelOnAutoInputRequestEvent(object sender, EventArgs e) {
            OpenDataDialogWindow(AutoInputWindowSettings,
                typeof(AutoInputViewModel), (AutoInputData o) => TemporarySettings.LastAutoInputData = o, SelectedProject.PintableViewModel.AutoInputRequestChanged);
        }

        private void PintableViewModelOnSearchPintableRequestEvent(object sender, EventArgs e) {
            OpenDataDialogWindow(SearchPintableWindowSettings, typeof(SearchPintableViewModel),
                (SearchPintableData o) => TemporarySettings.LastSearchedPintableData = o, SelectedProject.PintableViewModel.SearchPintableRequest);
        }

        private void PintableViewModelOnQuickInputRequestEvent(object sender, EventArgs e) {
            QuickInputRequestCommandMethod(null);
        }

        private ProjectViewModel CheckIfProjectIsAlreadyOpened(Project project) {
            return OpenedProjects.SingleOrDefault(x => x.Project.Equals(project));
        }

        private void SubscribeUpperMenuEvents() {
            UpperMenuCommands.NewProjectCommand = new RelayCommand<object>(UpperMenuOnNewProjectClick);
            UpperMenuCommands.OpenFolderCommand = new RelayCommand<object>(UpperMenuLoadFolder);
            UpperMenuCommands.SaveCommand = new RelayCommand<object>(UpperMenuOnSaveClick, (o) => (SelectedProject != null && SelectedProject.IsDirty));
            UpperMenuCommands.SaveAllCommand = new RelayCommand<object>(SaveAllProjectsCommandMethod, (o) => SelectedProject != null && OpenedProjects.Any(x => x.IsDirty));
            UpperMenuCommands.ProbeCommand = new RelayCommand<object>(ProbeCommandMethod, (o) => SelectedProject != null);
            UpperMenuCommands.CompileCommand = new RelayCommand<ProjectViewModel>(CompileProjectCommandMethod, (o) => SelectedProject != null);
        }

        private void SaveAllProjectsCommandMethod(object sender) {
            if (!OpenedProjects.Any()) return;
            foreach (var project in OpenedProjects) {
                project.SaveAsync();
            }
        }

        private void UpperMenuLoadFolder(object sender) {
            CommonOpenFileDialog dialog = new CommonOpenFileDialog() { IsFolderPicker = true };
            if (dialog.ShowDialog() != CommonFileDialogResult.Ok) return;
            OpenedProjects.Clear();
            SelectedProject = null;
            ProjectExplorerViewModel.Initialize(dialog.FileName);
        }

        private void UpperMenuOnNewProjectClick(object sender) {
            NewProjectViewModel _viewModel = new NewProjectViewModel(2);
            NewProjectWindowSettings.DataContext = _viewModel;
            NewProjectWindowSettings.IsOpen = true;
        }

        private async void UpperMenuOnSaveClick(object sender) {
            if (SelectedProject != null) await SelectedProject.SaveAsync();
        }

        private void ProbeCommandMethod(object obj) {
            if (SelectedProject == null) return;
            ApplicationEvents.SendRequestSwitchOffLeds();
            ProbeViewModel _viewModel = new ProbeViewModel(SelectedProject);
            _viewModel.ProbePinDetect += (sender, pin) => SelectedProject.PintableViewModel.SelectedItem = pin;
            ProbeWindowSettings.DataContext = _viewModel;
            ProbeWindowSettings.IsOpen = true;
            _viewModel.StartProbe();
        }

        public WindowSettings NewProjectWindowSettings { get; } = new WindowSettings() {
            IsItDialog = true,
            StartupLocation = WindowStartupLocation.CenterOwner,
            State = WindowState.Normal
        };

        public WindowSettings QuickInputWindowSettings { get; } = new WindowSettings() {
            IsItDialog = true,
            StartupLocation = WindowStartupLocation.CenterOwner,
            State = WindowState.Normal
        };

        public WindowSettings AutoInputWindowSettings { get; } = new WindowSettings() {
            IsItDialog = true,
            StartupLocation = WindowStartupLocation.CenterOwner,
            State = WindowState.Normal
        };

        public WindowSettings SearchPintableWindowSettings { get; } = new WindowSettings() {
            IsItDialog = true,
            StartupLocation = WindowStartupLocation.CenterOwner,
            State = WindowState.Normal
        };

        public WindowSettings ProbeWindowSettings { get; } = new WindowSettings() {
            IsItDialog = false,
            StartupLocation = WindowStartupLocation.CenterOwner,
            State = WindowState.Normal
        };

        #region Search pintable command

        public ICommand SearchPintableRequestCommand => new RelayCommand(SearchPintableRequestCommandMethod);
        private void SearchPintableRequestCommandMethod(object obj) {
            OpenDataDialogWindow(SearchPintableWindowSettings, typeof(SearchPintableViewModel),
                (SearchPintableData o) => TemporarySettings.LastSearchedPintableData = o, SelectedProject.PintableViewModel.SearchPintableRequest);
        }

        #endregion

        public ICommand CompileProjectCommand => new RelayCommand<object>(CompileProjectCommandMethod);
        private void CompileProjectCommandMethod(object obj) {
            Compile(SelectedProject.Project.Path);
        }

        private async void Compile(string path) {
            if (!await SelectedProject.SaveAsync()) return;
            bool _compile = ApplicationCommonSettings.DeviceManager.CompileProject(SelectedProject.Project.Path);
            if (_compile)
                MessageBox.Show("Compilation successfully.", "Compilation", MessageBoxButton.OK,
                    MessageBoxImage.Information);
            else {
                MessageBox.Show("Compilation failure.", "Compilation", MessageBoxButton.OK,
                    MessageBoxImage.Error);
            }
        }

        #region Quickinput Command
        public ICommand QuickInputRequestCommand => new RelayCommand(QuickInputRequestCommandMethod);
        private void QuickInputRequestCommandMethod(object obj) {
            Pin _firstDescribedPin = SelectedProject.PintableViewModel.FindFirstDescribedPinFromSelected();
            QuickInputViewModel _viewModel = new QuickInputViewModel(_firstDescribedPin);
            QuickInputWindowSettings.DataContext = _viewModel;
            QuickInputWindowSettings.IsOpen = true;
            if (!_viewModel.Dialog) return;
            TemporarySettings.LastQuickInputData = _viewModel.Data;
            SelectedProject.PintableViewModel.QuickInputRequest(_viewModel.Data);
        }
        #endregion


        #region Remove tab Command

        public ICommand RemoveProjectCommand => new RelayCommand<ProjectViewModel>(RemoveProjectCommandMethod);
        private void RemoveProjectCommandMethod(ProjectViewModel obj) {
            RemoveTabAsync(obj);
        }

        private async void RemoveTab(ProjectViewModel obj) {
            if (obj.IsDirty) {
                MessageBoxResult _result = MessageBox.Show($"Project '{obj.Title.Replace("*", "")}' has been changed. Do you want to save your changes?", "Exit",
                    MessageBoxButton.YesNoCancel, MessageBoxImage.Question);

                switch (_result) {
                    case MessageBoxResult.Cancel: return;
                    case MessageBoxResult.No: {
                            OpenedProjects.Remove(obj);
                            return;
                        }
                    default: {
                            await SelectedProject.SaveAsync();
                            OpenedProjects.Remove(obj);
                            return;
                        }
                }
            }

            OpenedProjects.Remove(obj);
        }

        private Task RemoveTabAsync(ProjectViewModel obj) {
            return Task.Factory.StartNew(() => RemoveTab(obj));
        }
        #endregion


        private void OpenDataDialogWindow<I>(WindowSettings settings, Type type, Action<I> updateAction, Action<I> action) {
            var viewModel = Activator.CreateInstance(type);
            if (viewModel is DataDialogBase<I> _base) {
                settings.DataContext = viewModel;
                settings.IsOpen = true;
                if (!_base.Dialog) return;
                updateAction(_base.Data);
                action.Invoke(_base.Data);
            } else {
                throw new InvalidOperationException("Invalid type");
            }


        }
    }
}
