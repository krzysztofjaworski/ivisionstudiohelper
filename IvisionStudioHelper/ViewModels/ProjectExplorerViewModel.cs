using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Security.Permissions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using CustomControls;
using HelperLibrary.MVVM;
using HelperLibrary.Utils;
using IvisionStudioHelper.Model;
using IvisionStudioHelper.Model.Project;
using IvisionStudioHelper.Model.Settings;

namespace IvisionStudioHelper.ViewModels {
    public class ProjectExplorerViewModel : PropertyChangedBase {
        public AsyncObservableCollection<Project> Headers { get; set; } = new AsyncObservableCollection<Project>();
        public event EventHandler<Project> ProjectIsSelected;
        public bool IsLoading { get; set; } = false;
        public double Percent { get; set; } = 0;

        public async void Initialize(string path) {
            Headers.Clear();
            if (!System.IO.Directory.Exists(path)) throw new InvalidOperationException("The directory does not exist");
            if (Headers.Any()) Headers.Clear();
            List<string> _projectsFiles = System.IO.Directory.GetFiles(path, "*.project").ToList();
            if (!_projectsFiles.Any()) return;
            IsLoading = true;
            using (TimeMeasurement _measurement = new TimeMeasurement($"Loading {_projectsFiles.Count} elements.")) {
                double _done = 0;
                foreach (string projectsFile in _projectsFiles) {
                    Project project = new Project(projectsFile);
                    await project.InitializeAsync();
                    Headers.Add(project);
                    Percent = Math.Round((++_done / _projectsFiles.Count) * 100, 2);
                }                
            }
            IsLoading = false;
            Percent = 0;
            ConfigurationManager.ApplicationSettings.LastOpenedPath = path;
            ConfigurationManager.Save();
        }

        public Task InitializeAsync(string path) {
            return Task.Factory.StartNew(() => Initialize(path));
        }

        public ICommand DoubleClickProjectCommand => new RelayCommand(DoubleClickProjectCommandMethod);

        private void DoubleClickProjectCommandMethod(object obj) {
            if (obj is Project project) {
                ProjectIsSelected?.Invoke(this, project);
            }
        }
    }
}