using System.Windows.Input;

namespace IvisionStudioHelper.ViewModels.Commands {
    public static class UpperMenuCommands {
        public static ICommand NewProjectCommand { get; set; }
        public static ICommand OpenFolderCommand { get; set; }
        public static ICommand SaveCommand { get; set; }
        public static ICommand SaveAllCommand { get; set; }
        public static ICommand ProbeCommand { get; set; }
        public static ICommand CompileCommand { get; set; }
}
    }