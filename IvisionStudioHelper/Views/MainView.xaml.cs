﻿using System.Windows;
using System.Windows.Controls;

namespace IvisionStudioHelper.Views {
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainView {
        public MainView() {
            InitializeComponent();
            Closed += (sender, args) => Application.Current.Shutdown();
        }
        
        /// <summary>
        /// Tworzy nową instancję DataTemplate ponieważ w standarowej kontrolce TabControl jest bug który tworzy instancję tylko raz.
        /// </summary>
        private void TabItem_Loaded(object sender, RoutedEventArgs e) {
            TabItem tabItem = (sender as TabItem);

            if (null != tabItem.DataContext) {
                var dataTemplate = (DataTemplate)this.FindResource("ProjectViewModelTemplate");
                var cp = new ContentPresenter();
                cp.ContentTemplate = dataTemplate;
                cp.Content = tabItem.DataContext;
                tabItem.Content = cp;
            }
        } 
        
    }       
}