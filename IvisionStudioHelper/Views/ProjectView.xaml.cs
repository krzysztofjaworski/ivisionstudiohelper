using System;
using System.Collections.Generic;
using System.Windows.Controls;
using System.Windows.Input;
using ICSharpCode.AvalonEdit;
using ICSharpCode.AvalonEdit.CodeCompletion;
using ICSharpCode.AvalonEdit.Document;
using ICSharpCode.AvalonEdit.Editing;

namespace IvisionStudioHelper.Views {
    public partial class ProjectView : UserControl {
        public ProjectView() {
            InitializeComponent();
            if (TSLEditor is TextEditor _editor) {
                TextEditor = _editor;
                _editor.TextArea.TextEntering += TextAreaOnTextEntering;
                _editor.TextArea.KeyDown += TextAreaOnPreviewKeyDown;    
            }
        }

        private TextEditor TextEditor;

        private void TextAreaOnPreviewKeyDown(object sender, KeyEventArgs e) {
           
            
            if ((Keyboard.Modifiers & ModifierKeys.Control) == ModifierKeys.Control && e.Key == Key.Space) {
                completionWindow = new CompletionWindow(TextEditor.TextArea);
                IList<ICompletionData> data = completionWindow.CompletionList.CompletionData;
                data.Add( new MyCompletionData("if"));
                data.Add( new MyCompletionData("else"));
                data.Add( new MyCompletionData("CallCommandBlock"));
                data.Add( new MyCompletionData("CommandBlock"));
                data.Add( new MyCompletionData("CommandBlockTestEnd"));
                data.Add( new MyCompletionData("CommandBlockTestStart"));
                data.Add( new MyCompletionData("CreateBarCodeImage"));
                data.Add( new MyCompletionData("CreateBarCodeImageFile"));
                data.Add( new MyCompletionData("DefineDetection"));
                data.Add( new MyCompletionData("DefineRandomWiring"));
                data.Add( new MyCompletionData("DefSec"));
                data.Add( new MyCompletionData("DisableAutomaticIsolationTest")); 
                data.Add( new MyCompletionData("DoIsolationTest"));
                data.Add( new MyCompletionData("DoUnplugTest"));
                data.Add( new MyCompletionData("end"));
                data.Add( new MyCompletionData("ExcludePinsFromIsolationTest")); 
                data.Add( new MyCompletionData("FileDirectoryExists"));
                data.Add( new MyCompletionData("FileExists"));
                data.Add( new MyCompletionData("FileReadIni"));
                data.Add( new MyCompletionData("FileWriteIni"));
                data.Add( new MyCompletionData("GetDate"));
                data.Add( new MyCompletionData("GetDateTime"));
                data.Add( new MyCompletionData("GetTime"));
                data.Add( new MyCompletionData("GetValueFromData"));   
                data.Add( new MyCompletionData("IfAllModules"));
                data.Add( new MyCompletionData("IfAtLeastOneModule"));
                data.Add( new MyCompletionData("IfNoModule"));
                data.Add( new MyCompletionData("IfOnlyOneModule"));
                data.Add( new MyCompletionData("Include"));
                data.Add( new MyCompletionData("LEDSetAllOff"));
                data.Add( new MyCompletionData("LEDSetAllOn"));
                data.Add( new MyCompletionData("LEDSetOff"));
                data.Add( new MyCompletionData("LEDSetOn"));
                data.Add( new MyCompletionData("LEDSetUsedOn"));
                data.Add( new MyCompletionData("MeasureCapacitance"));
                data.Add( new MyCompletionData("MeasureExternalCurrentToGround"));  
                data.Add( new MyCompletionData("MeasureExternalVoltageToGround"));
                data.Add( new MyCompletionData("MeasureResistance"));
                data.Add( new MyCompletionData("MeasureResistanceFourTerminalMeasurement")); 
                data.Add( new MyCompletionData("NoConnAll"));
                data.Add( new MyCompletionData("NoConnectionLV"));
                data.Add( new MyCompletionData("NoConnectionNetwork"));
                data.Add( new MyCompletionData("NoConnGroup"));
                data.Add( new MyCompletionData("NoConnLower"));
                data.Add( new MyCompletionData("ParamAutostartBlock")); 
                data.Add( new MyCompletionData("ParamAutostartComponent"));
                data.Add( new MyCompletionData("ParamAutostartContinuity"));
                data.Add( new MyCompletionData("ParamAutostartIsolation"));
                data.Add( new MyCompletionData("ParamContinuity"));
                data.Add( new MyCompletionData("ParamIsolation"));
                data.Add( new MyCompletionData("ParamMiswireCheck"));
                data.Add( new MyCompletionData("ParamResetContinuity"));
                data.Add( new MyCompletionData("ParamResetIsolation"));
                data.Add( new MyCompletionData("POFEject"));
                data.Add( new MyCompletionData("POFGetInputSignal")); 
                data.Add( new MyCompletionData("POFSetOutputSignal"));
                data.Add( new MyCompletionData("PowerPinAllOff"));
                data.Add( new MyCompletionData("PowerPinLowOff"));
                data.Add( new MyCompletionData("PowerPinLowOn"));
                data.Add( new MyCompletionData("PrintLabel"));
                data.Add( new MyCompletionData("RemoteGet"));
                data.Add( new MyCompletionData("RemoteSet"));
                data.Add( new MyCompletionData("RemoteVerify"));
                data.Add( new MyCompletionData("RS232Close"));
                data.Add( new MyCompletionData("RS232Open"));
                data.Add( new MyCompletionData("RS232Read"));
                data.Add( new MyCompletionData("RS232Write"));
                data.Add( new MyCompletionData("SetConnectorDetection")); 
                data.Add( new MyCompletionData("StopButtonLocked"));
                data.Add( new MyCompletionData("StopwatchGet"));
                data.Add( new MyCompletionData("StopwatchStart"));
                data.Add( new MyCompletionData("StrAdd"));
                data.Add( new MyCompletionData("StrCompare"));
                data.Add( new MyCompletionData("StrCopy"));
                data.Add( new MyCompletionData("StrDelete"));
                data.Add( new MyCompletionData("StrFormat"));
                data.Add( new MyCompletionData("StrInsert"));
                data.Add( new MyCompletionData("StrLength"));
                data.Add( new MyCompletionData("StrLowerCase"));
                data.Add( new MyCompletionData("StrPosition"));
                data.Add( new MyCompletionData("StrToDouble"));
                data.Add( new MyCompletionData("StrTrim"));
                data.Add( new MyCompletionData("StrTrimLeft"));
                data.Add( new MyCompletionData("StrTrimRight"));
                data.Add( new MyCompletionData("StrUpperCase"));
                data.Add( new MyCompletionData("switch"));
                data.Add( new MyCompletionData("TestAllConnectorDetections"));
                data.Add( new MyCompletionData("TestAllDetections"));
                data.Add( new MyCompletionData("TestCapacitor"));
                data.Add( new MyCompletionData("TestComponent"));
                data.Add( new MyCompletionData("TestConnection"));
                data.Add( new MyCompletionData("TestConnectorDetection"));
                data.Add( new MyCompletionData("TestDetection"));
                data.Add( new MyCompletionData("TestDiode"));
                data.Add( new MyCompletionData("TestExternalCurrentToGround"));
                data.Add( new MyCompletionData("TestExternalVoltage"));
                data.Add( new MyCompletionData("TestExternalVoltageToGround"));
                data.Add( new MyCompletionData("TestNetwork"));
                data.Add( new MyCompletionData("TestPOF"));
                data.Add( new MyCompletionData("TestPosition"));
                data.Add( new MyCompletionData("TestProbe"));
                data.Add( new MyCompletionData("TestRandomWiring"));
                data.Add( new MyCompletionData("TestResistor"));
                data.Add( new MyCompletionData("TestResistorFourTerminalMeasurement"));
                data.Add( new MyCompletionData("TestSwitch"));
                data.Add( new MyCompletionData("TestZDiode"));
                data.Add( new MyCompletionData("U1PinAllOff"));
                data.Add( new MyCompletionData("U1PinHighAllOff"));
                data.Add( new MyCompletionData("U1PinHighOff"));
                data.Add( new MyCompletionData("U1PinHighOn"));
                data.Add( new MyCompletionData("U1PinLowAllOff"));
                data.Add( new MyCompletionData("U1PinLowOff"));
                data.Add( new MyCompletionData("U1PinLowOn"));
                data.Add( new MyCompletionData("UIMessage"));
                data.Add( new MyCompletionData("UIMessageConfirm")); 
                data.Add( new MyCompletionData("UIMessageConfirmMedia"));
                data.Add( new MyCompletionData("UIMessagePanelClose"));
                data.Add( new MyCompletionData("UIMessagePanelError"));
                data.Add( new MyCompletionData("UIMessagePanelInfo"));
                data.Add( new MyCompletionData("UIMessagePanelMedia"));
                data.Add( new MyCompletionData("UIMessageSetVariable"));
                data.Add( new MyCompletionData("UIMessageStop"));
                data.Add( new MyCompletionData("UIMessageStopMedia"));
                data.Add( new MyCompletionData("UIMessageVerify"));
                data.Add( new MyCompletionData("Wait"));
                data.Add( new MyCompletionData("while"));
                completionWindow.Width = 300;
                completionWindow.Show();
                completionWindow.Closed += (o, args) => completionWindow = null;
            }
        }

        CompletionWindow completionWindow;
        
        private void TextAreaOnTextEntering(object sender, TextCompositionEventArgs e) {
            if ((Keyboard.Modifiers & ModifierKeys.Control) == ModifierKeys.Control && Keyboard.IsKeyDown(Key.Space)) {
                e.Handled = true;
            }
        }
    }
    
    public class MyCompletionData : ICompletionData
    {
        public MyCompletionData(string text)
        {
            this.Text = text;
        }

        public System.Windows.Media.ImageSource Image {
            get { return null; }
        }

        public string Text { get; private set; }

        // Use this property if you want to show a fancy UIElement in the list.
        public object Content {
            get { return this.Text; }
        }

        public object Description {
            get { return "Description for " + this.Text; }
        }

        public double Priority { get; }

        public void Complete(TextArea textArea, ISegment completionSegment,
            EventArgs insertionRequestEventArgs)
        {
            textArea.Document.Replace(completionSegment, this.Text + "( ");
        }
    }
}
