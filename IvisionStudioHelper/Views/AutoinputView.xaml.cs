﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace IvisionStudioHelper.Views
{
    /// <summary>
    /// Interaction logic for AutoinputView.xaml
    /// </summary>
    public partial class AutoinputView : Window
    {
        public AutoinputView()
        {
            InitializeComponent();
            Loaded += (sender, args) => {
                TraversalRequest tRequest = new TraversalRequest(FocusNavigationDirection.Next);
                this.MoveFocus(tRequest);
            };            
        }
    }
}
