using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using HelperLibrary.Extensions;

namespace IvisionStudioHelper.Views.Attached {
    public class EditCellAfterEnter : DependencyObject {

        public static bool GetEditCell(DependencyObject obj) {
            return (bool)obj.GetValue(EditCellProperty);
        }

        public static void SetEditCell(DependencyObject obj, bool value) {
            obj.SetValue(EditCellProperty, value);
        }

        public static readonly DependencyProperty EditCellProperty =
            DependencyProperty.RegisterAttached("EditCell", typeof(bool), typeof(EditCellAfterEnter), new FrameworkPropertyMetadata(false, PropertyChanged));

        private static void PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e) {
            if (d is DataGrid _datagrid) {
                _datagrid.PreviewKeyDown += DatagridOnPreviewKeyDown;
            }
        }

        private static void DatagridOnPreviewKeyDown(object sender, KeyEventArgs e) {
            DataGrid _dataGrid = (DataGrid)sender;
            if (e.Key != Key.Enter) return;
            e.Handled = true;
            bool _shift = (Keyboard.Modifiers & ModifierKeys.Shift) != 0;

            if (!_shift) {
                if (_dataGrid.Items.Count <= _dataGrid.SelectedIndex + 1) return;
            } else if (_dataGrid.SelectedIndex - 1 <= -1) return;

            DataGridRow _item = (DataGridRow)_dataGrid.ItemContainerGenerator.ContainerFromItem(_dataGrid.ItemContainerGenerator.Items[_dataGrid.SelectedIndex + (_shift ? -1 : 1)]);
            DataGridCellsPresenter _presenter = _item.FindVisualChild<DataGridCellsPresenter>();
            DataGridCell _cell = (DataGridCell)_presenter.ItemContainerGenerator.ContainerFromIndex(_dataGrid.Columns.IndexOf(_dataGrid.CurrentColumn));
            if (_cell == null) return;
            _dataGrid.SelectedItems.Clear();
            _item.IsSelected = true;
            _cell.Focus();
            _dataGrid.BeginEdit();
        }
    }
}