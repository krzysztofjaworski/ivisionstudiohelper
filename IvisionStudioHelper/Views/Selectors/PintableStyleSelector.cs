﻿using IvisionStudioHelper.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using IvisionStudioHelper.Model.Pintable;

namespace IvisionStudioHelper.Views.Selectors {
    public class PintableStyleSelector : StyleSelector {
        public Style EndBoard { get; set; }

        public override Style SelectStyle(object item, DependencyObject container) {
            if (container is DataGridRow _row && _row.DataContext is Pin _pin)
                if ((_pin.System % 64) == 0) return EndBoard;

            return base.SelectStyle(item, container);
        }
    }
}
