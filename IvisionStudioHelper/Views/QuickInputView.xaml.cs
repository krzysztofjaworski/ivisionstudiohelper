﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace IvisionStudioHelper.Views
{
    /// <summary>
    /// Interaction logic for QuickInputView.xaml
    /// </summary>
    public partial class QuickInputView : Window
    {
        public QuickInputView()
        {
            InitializeComponent();
            Loaded += (sender, args) => {
                TraversalRequest tRequest = new TraversalRequest(FocusNavigationDirection.Next);
                    MoveFocus(tRequest);
                    UIElement _element = Keyboard.FocusedElement as UIElement;
                    _element.MoveFocus(tRequest);
                    _element = Keyboard.FocusedElement as UIElement;
                    _element.MoveFocus(tRequest);
            };   
        }
    }
}
