using System.Windows;
using System.Windows.Input;

namespace IvisionStudioHelper.Views {
    public partial class SearchPintableView : Window {
        public SearchPintableView() {
            InitializeComponent();
            Loaded += (sender, args) => {
                TraversalRequest tRequest = new TraversalRequest(FocusNavigationDirection.Next);
                this.MoveFocus(tRequest);
            };  
        }
    }
}
