using System;
using System.Windows.Controls;

namespace IvisionStudioHelper.Views {
    public partial class PintableView : UserControl {
        public PintableView() {
            InitializeComponent();
        }

        private void Selector_OnSelectionChanged(object sender, SelectionChangedEventArgs e) {
            if (sender is DataGrid _datagrid && _datagrid.SelectedItem != null && (e.OriginalSource?.Equals(_datagrid) ?? false))
                _datagrid.ScrollIntoView(_datagrid.SelectedItem, null);
        }
    }
}
