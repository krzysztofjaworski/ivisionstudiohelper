using System;
using System.Globalization;
using System.Windows.Data;
using IvisionStudioHelper.Model;
using IvisionStudioHelper.Model.Pintable;

namespace IvisionStudioHelper.Views.Converters {
    public class IsItLedConverter : IValueConverter {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
            if (value is Pin _pin) return _pin.Type;

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
            throw new NotImplementedException();
        }
    }
}